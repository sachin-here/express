
var app = require('../../examples/hello-world')
var request = require('supertest')
var assert = require('assert');

describe('hello-world', function () {
  describe('GET /', function () {
    it('should respond with hello world', function (done) {
      request(app)
        .get('/')
        .expect(200, 'Hello World', done)
    })
  })

  describe('flaky-test', function () {
    it('sample-test', function () {
      var x = Math.round((Math.random() + 100))%2
      assert.equal(x, 1, "flaky test failed failed, expected: x=1");
    })
    it('sample-test', function () {
      var today = new Date();
      today = today.getSeconds();
      assert.equal(today%2, 1, "flaky test failed failed");
    })
  })

  describe('GET /missing', function () {
    it('should respond with 404', function (done) {
      request(app)
        .get('/missing')
        .expect(404, done)
    })
  })
})
